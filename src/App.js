
import "./App.css";

import { Tooltip, Whisper, Button } from "rsuite";
function App() {

  return (
    <Whisper
    followCursor
    speaker={<Tooltip>This is a Tooltip that follow cursor</Tooltip>}
  >
    <Button><span>Hover me</span></Button>
  </Whisper>

  )
}
export default App;
